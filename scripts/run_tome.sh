#!/bin/bash -ex
#SBATCH -J TOME
#SBATCH -t 5:00:00
#SBATCH -c 1
#SBATCH --mem=200G
#SBATCH -o /g/arendt/npapadop/cluster/TOME_%j.out
#SBATCH -e /g/arendt/npapadop/cluster/TOME_%j.err

# needed file list
SCRIPT="/g/arendt/npapadop/repos/platython/scripts/tome_script.py"

module load Anaconda3/2020.07
CONDA_BASE=$(conda info --base)
CONFIG=$1

source ${CONDA_BASE}/etc/profile.d/conda.sh
conda activate /g/arendt/npapadop/repos/condas/scanpy
which python
python ${SCRIPT} --config "${CONFIG}"

conda deactivate
module unload Anaconda3/2020.07
