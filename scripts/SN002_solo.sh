#!/bin/bash -ex
#SBATCH -J solo
#SBATCH -t 20:00:00
#SBATCH -c 10
#SBATCH --mem=40G
#SBATCH -o /g/arendt/npapadop/cluster/solo_SN002.out
#SBATCH -e /g/arendt/npapadop/cluster/solo_SN002.err

# needed file list
SCRIPT="/g/arendt/npapadop/repos/platython/scripts/SN002_solo.py"

module load Anaconda3/2020.07
CONDA_BASE=$(conda info --base)

source ${CONDA_BASE}/etc/profile.d/conda.sh
conda activate /g/arendt/npapadop/repos/condas/scanpy

python ${SCRIPT}

conda deactivate /g/arendt/npapadop/repos/condas/scanpy
module unload Anaconda3/2020.07