#!/usr/bin/env python

from datetime import datetime
import argparse
import pickle
from pathlib import Path
import warnings
from tqdm import tqdm

from numba.core.errors import NumbaPerformanceWarning
import numpy as np
import scanpy as sc
import scipy

import harmonypy as hm

warnings.simplefilter(action='ignore', category=FutureWarning)
warnings.filterwarnings("ignore", category=NumbaPerformanceWarning)


def cluster_pipeline(adata, n_comps, clust_res, k, cluster_key="leiden"):
    adata.X = adata.layers["counts"].copy()

    # Normalize adata 
    adata.X /= adata.obs['size_factors'].values[:,None]
    # immediately convert to csr else we have a matrix object at tidy.raw.X later
    adata.X = scipy.sparse.csr_matrix(adata.X)
    sc.pp.log1p(adata)

    adata.raw = adata

    sc.pp.highly_variable_genes(adata, n_top_genes=2000)
    sc.pp.scale(adata, zero_center=False)
    sc.pp.pca(adata, n_comps=n_comps, use_highly_variable=True, svd_solver='arpack')

    ho = hm.run_harmony(adata.obsm["X_pca"], adata.obs, ["batch"])

    adata.obsm["X_harmony"] = ho.Z_corr.T[:, :20]

    sc.pp.neighbors(adata, metric="cosine", use_rep="X_harmony", n_neighbors=k)
    sc.tl.leiden(adata, resolution=clust_res, key_added=cluster_key)

def cluster_to_jaccard(goal, prediction):
    true_length = len(np.unique(goal))
    pred_length = len(np.unique(prediction))
    tabulated = np.zeros((true_length, pred_length))
    for (i, j) in zip(goal, prediction):
        tabulated[int(i), int(j)] += 1
        
    true_clusters, true_freq = np.unique(goal, return_counts=True)
    pred_clusters, pred_freq = np.unique(prediction, return_counts=True)
    for i, freq_i in zip(true_clusters, true_freq):
        for j, freq_j in zip(pred_clusters, pred_freq):
            x = int(i)
            y = int(j)
            tabulated[x, y] /= freq_i + freq_j - tabulated[x, y]
    return tabulated

def calculate_stability(adata, n_comps, clust_res, k, replicates=20, fraction=0.8):
    stability = np.zeros((replicates, len(adata.obs.leiden.cat.categories)))
    prev_index = None
    for r in tqdm(range(replicates)):
        # subsample the data
        subset = sc.pp.subsample(adata, fraction=fraction, copy=True, random_state=r)
        # cluster the subsampled data
        cluster_pipeline(subset, n_comps, clust_res, k, cluster_key="predict")
        # compare the subsampled clustering to the the real one and keep the best
        # matching subsampled cluster
        goal = subset.obs["leiden"].values
        pred = subset.obs["predict"].values
        overview = cluster_to_jaccard(goal, pred)
        stability[r] = np.max(overview, axis=1)
    return stability

def evaluate_clustering(adata, stability, stability_cutoff = 0.6):
    clusters, freq = np.unique(adata.obs.leiden.astype(int), return_counts=True)
    stable_clusters = np.mean(stability, axis=0) > stability_cutoff
    cells_in_stable = np.sum(freq[stable_clusters])

    return stable_clusters, cells_in_stable


def main(ref_loc, n_comps, clust_res, k, out_loc):
    print(datetime.today().strftime('%Y-%m-%d'))
    reference = sc.read(ref_loc)
    key = f"n{n_comps}-r{clust_res}-k{k}"
    out_loc = f"{out_loc}/{key}"
    Path(out_loc).mkdir(parents=True, exist_ok=True)
    print(key)
    adata = reference.copy()
    cluster_pipeline(adata, n_comps, clust_res, k, cluster_key="leiden")
    stability = calculate_stability(adata, n_comps, clust_res, k)
    stable_clusters, cells_in_stable = evaluate_clustering(adata, stability)
    reference.obs[key] = adata.obs["leiden"]
    adata.write(f"{out_loc}/adata.h5ad")
    with open(f"{out_loc}/stable_clusters.pkl", "wb") as f:
        pickle.dump(stable_clusters, f)
    with open(f"{out_loc}/cells_in_stable.pkl", "wb") as f:
        pickle.dump(cells_in_stable, f)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Test the cluster stability of a certain parameter configuration via Jaccard index."
    )

    parser.add_argument(
        "-i",
        dest="reference",
        metavar="input.h5ad",
        type=str,
        nargs=1,
        help="location of the input file",
    )

    parser.add_argument(
        "-o",
        dest="output",
        metavar="output_dir/",
        type=str,
        nargs=1,
        help="location of the output",
    )

    parser.add_argument(
        "-n",
        dest="n_comps",
        metavar="N",
        type=int,
        nargs=1,
        help="the number of principal components to use as input to Harmony",
    )

    parser.add_argument(
        "-k",
        dest="k",
        metavar="N",
        type=int,
        nargs=1,
        help="The number of nearest neighbours to consider when calculating cell/cell distances",
    )

    parser.add_argument(
        "-r",
        dest="clust_res",
        metavar="float",
        type=float,
        nargs=1,
        help="the resolution for Leiden clustering",
    )

    args = parser.parse_args()
    out_loc = args.output[0]
    ref_loc = args.reference[0]
    n_comps = args.n_comps[0]
    clust_res = args.clust_res[0]
    k = args.k[0]
    main(ref_loc, n_comps, clust_res, k, out_loc)