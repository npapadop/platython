#!/bin/bash -ex
#SBATCH -J merge_clusters
#SBATCH -t 10:00:00
#SBATCH -c 1
#SBATCH --mem=50G
#SBATCH -o /g/arendt/npapadop/cluster/merge_clusters_%j.out
#SBATCH -e /g/arendt/npapadop/cluster/merge_clusters_%j.err

# needed file list
SCRIPT="/g/arendt/npapadop/repos/platython/scripts/merge_clusters.py"

module load Anaconda3/2019.07
CONDA_BASE=$(conda info --base)
INPUT="/g/arendt/npapadop/repos/platython-notebook/write/pd_6dpf_nuclei_jake.h5ad"
OUTPUT="/g/arendt/npapadop/repos/platython-notebook/write/pd_6dpf_nuclei_louvain_merged.h5ad"

source ${CONDA_BASE}/etc/profile.d/conda.sh
conda activate /g/arendt/npapadop/repos/condas/scanpy
python ${SCRIPT} ${INPUT} louvain -o ${OUTPUT} --verbose

conda deactivate /g/arendt/npapadop/repos/condas/nikonda
module unload Anaconda3/2019.07