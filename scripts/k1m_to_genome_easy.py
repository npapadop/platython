#!/usr/bin/env python

import numpy as np
import pandas as pd
import argparse


def annotate_k1m(k1m, index, freq, mrna):
    #     k1m, index, freq, ns = arg
    genes = mrna[index : (index + freq)]["gene_index"]
    possible_genes = mrna[index : (index + freq)]["gene_index"]
    unique_genes = np.unique(possible_genes)
    # simplest case: this fragment has mapped exclusively to
    # one draft gene
    if (len(unique_genes) == 1) and unique_genes[0] != -1:
        return unique_genes[0]
    else:
        # subset the matrix to look at all hits of this gene
        test = mrna[mrna.trinity == k1m]
        # remove hits that are in non-coding regions
        # this is kinda safe because with scRNA-seq we are
        # fishing for mRNA anyway, so it should be in coding regions.
        test = test[test.gene_index != -1]

        # count how many times each draft gene comes up as a hit
        test_freq = pd.DataFrame(np.unique(test.gene_index, return_counts=True)).T
        test_freq.index = test_freq[0]
        # transform that to weights. We will use it to give
        # genes that are hit more often an advantage.
        # Many of the true hits will be lower value (coverage/identity/matches).
        # These would drag the average performance of this draft gene down.
        # By weighing them with the occurrence frequency we try to tone
        # this effect down a bit.
        weights = test_freq[1] / np.sum(test_freq[1])
        test_mean = test.groupby(["gene_index"]).mean()
        decision = (
            test_mean.multiply(weights, axis="rows")
            .iloc[:, 2:4]
            .sort_values(["identity", "coverage"], ascending=False)
        )
        # if there is nothing here this means this k1m fragment only maps against
        # non-coding/unannotated regions; keep the k1m ID.
        if decision.shape[0] == 0:
            return k1m
        # if there is only one draft gene left (i.e. the k1m fragment mapped
        # against a draft gene and some uncoding regions), then keep that fragment,
        # provided it meets a minimum standard of quality. Otherwise, keep the k1m ID.
        elif decision.shape[0] == 1:
            if (decision.iloc[0].coverage > 90) and (decision.iloc[0].identity > 90):
                return decision.index[0]
            else:
                return k1m
        # if there are multiple draft gene hits, keep the best one if it is
        # clearly better than all the others.
        elif all(decision.iloc[0] - decision.iloc[1] > 30):
            return decision.index[0]
        else:
            return k1m


def main(start, end, output):
    mrna_loc = (
        "/g/arendt/npapadop/repos/platython-notebook/genome/GMAP_Platy_tXOME_mapped.csv"
    )
    mRNA = pd.read_csv(mrna_loc, index_col=0)
    K1M_ids, indices, freq = np.unique(
        mRNA.trinity, return_index=True, return_counts=True
    )
    res = {}
    print("Summarising positions %d to %d" % (start, end))
    for k1m, i, f in zip(K1M_ids[start:end], indices[start:end], freq[start:end]):
        res[k1m] = annotate_k1m(k1m, i, f, mRNA)
    result = pd.DataFrame([res.keys(), res.values()]).T
    result.columns = ["k1m", "draft"]
    result.to_csv("%s/res_%d_%d.csv" % (output, start, end))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Map unique K1M IDs to the draft genome"
    )
    parser.add_argument(
        "-o",
        dest="output",
        metavar="output_dir/",
        type=str,
        nargs=1,
        help="location of the output",
    )
    parser.add_argument(
        "--from",
        dest="start",
        metavar="i",
        type=int,
        nargs=1,
        help="unique ID to start from",
    )
    parser.add_argument(
        "--to",
        dest="end",
        metavar="j",
        type=int,
        nargs=1,
        help="unique ID to end at",
    )

    args = parser.parse_args()
    output = args.output[0]
    start = args.start[0]
    end = args.end[0]
    main(start, end, output)