import scanpy as sc
from solo import hashsolo

SN002_loc = "/g/arendt/data/transcriptome_sequencing/pdum/10x/SN002_pdum-v2.11/outs/filtered_feature_bc_matrix.h5"
SN002 = sc.read_10x_h5(SN002_loc)
sc.pp.filter_cells(SN002, min_genes = 200)
sc.pp.filter_genes(SN002, min_cells = 2)
hashsolo.hashsolo(SN002)

SN002.write_h5ad("/g/arendt/npapadop/repos/platython-notebook/6dpf_pdum-v20/data/SN002_soloed.h5ad")