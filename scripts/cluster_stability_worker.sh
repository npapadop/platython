#!/bin/bash -ex
#SBATCH -J stability
#SBATCH -t 00:30:00
#SBATCH -c 1
#SBATCH --mem=20G
#SBATCH -o /g/arendt/npapadop/cluster/stability_%j.out
#SBATCH -e /g/arendt/npapadop/cluster/stability_%j.err

# needed file list
SCRIPT="/g/arendt/npapadop/repos/platython/scripts/cluster_stability.py"

module load Anaconda3/2020.07
CONDA_BASE=$(conda info --base)
INPUT=$1
OUTPUT=$2
NCOMPS=$3
K=$4
RES=$5

source ${CONDA_BASE}/etc/profile.d/conda.sh
conda activate /g/arendt/npapadop/repos/condas/scanpy

python ${SCRIPT} -i ${INPUT} -o ${OUTPUT} -n ${NCOMPS} -k ${K} -r ${RES}

conda deactivate /g/arendt/npapadop/repos/condas/scanpy
module unload Anaconda3/2020.07