#!/bin/bash -ex
#SBATCH -J phil_merge
#SBATCH -t 48:00:00
#SBATCH -N 1
#SBATCH -n 8
#SBATCH --mem=200G
#SBATCH -o /g/arendt/npapadop/cluster/phil_merge_%j.out
#SBATCH -e /g/arendt/npapadop/cluster/phil_merge_%j.err
#SBATCH --mail-user=npapadop@embl.de

/g/funcgen/bin/Rscript-3.6.2 /g/arendt/Phil/Platynereis_dumerilii/34_LIbrary_Merging_Factory_ft_Sept2020_revolutions/scripts_for_cluster/20201006_clustering_SC105-SN004.R