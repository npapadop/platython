#!/usr/bin/bash

# just a script to mass-submit the TOME jobs
SCRIPT="/g/arendt/npapadop/repos/platython/scripts/run_tome.sh"
PDUM24_48="/g/arendt/npapadop/repos/platython/scripts/config_24hpf_48hpf.yml"
PDUM48_72="/g/arendt/npapadop/repos/platython/scripts/config_48hpf_72hpf.yml"
PDUM72_96="/g/arendt/npapadop/repos/platython/scripts/config_72hpf_96hpf.yml"
PDUM96_120="/g/arendt/npapadop/repos/platython/scripts/config_96hpf_120hpf.yml"

sbatch ${SCRIPT} ${PDUM24_48}
sbatch ${SCRIPT} ${PDUM48_72}
sbatch ${SCRIPT} ${PDUM72_96}
sbatch ${SCRIPT} ${PDUM96_120}
