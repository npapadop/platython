#!/usr/bin/env python

import numpy as np
import pandas as pd
import argparse

from platython import shrink


def main(contig, output):
    mrna_loc = (
        "/g/arendt/npapadop/repos/platython-notebook/genome/GMAP_Platy_tXOME_mapped.csv"
    )
    mRNA = pd.read_csv(mrna_loc, index_col=0)
    K1M_ids, indices, freq = np.unique(
        mRNA.trinity, return_index=True, return_counts=True
    )
    unidentified = mRNA[(mRNA.coverage > 95) & (mRNA.identity > 95)]
    unidentified = unidentified[(unidentified.end - unidentified.start) < 10000]
    subset_contig = unidentified[unidentified.contig == contig]
    contig_sorted = subset_contig.sort_values("start")
    component = shrink.find_connected_components(contig_sorted)
    contig_sorted["component"] = component
    summary = shrink.summarize_contig(contig_sorted)
    summary = shrink.connectivity(summary)
    result = shrink.unpack_summary(summary, contig)

    result.to_csv("%s/res_%s.csv" % (output, contig))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Map unique K1M IDs to the draft genome"
    )
    parser.add_argument(
        "-o",
        dest="output",
        metavar="output_dir/",
        type=str,
        nargs=1,
        help="location of the output",
    )
    parser.add_argument(
        "-c",
        dest="contig",
        metavar="contig_ID",
        type=str,
        nargs=1,
        help="contig ID",
    )

    args = parser.parse_args()
    output = args.output[0]
    contig = args.contig[0]
    main(contig, output)