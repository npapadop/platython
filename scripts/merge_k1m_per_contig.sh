#!/bin/bash -ex
#SBATCH -J k1m2draft
#SBATCH -t 0:05:00
#SBATCH -c 1
#SBATCH --mem=200M
#SBATCH -o /g/arendt/npapadop/cluster/merge_k1m_%j.out
#SBATCH -e /g/arendt/npapadop/cluster/merge_k1m_%j.err
#SBATCH --qos=low

# needed file list
SCRIPT="/g/arendt/npapadop/repos/platython/scripts/merge_k1m_per_contig.py"

module load Anaconda3/2020.07
CONDA_BASE=$(conda info --base)
OUTPUT="/g/arendt/npapadop/data/single_cell/10x/k1m_to_draft/"

source ${CONDA_BASE}/etc/profile.d/conda.sh
conda activate /g/arendt/npapadop/repos/condas/scanpy
which python
python ${SCRIPT} -o ${OUTPUT} -c "$contig"

conda deactivate
module unload Anaconda3/2020.07