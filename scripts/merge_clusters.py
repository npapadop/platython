#!/usr/bin/env python

# first mute future warnings and only then import pandas
import datetime
import warnings
import argparse

warnings.simplefilter(action="ignore", category=FutureWarning)
from threadpoolctl import threadpool_limits

import numpy as np
import pandas as pd
import scanpy as sc
import anndata as ad

from tqdm import tqdm

from platython.cluster import *


def main(
    file_in,
    original_clustering,
    out=None,
    num_genes=20,
    min_cells=10,
    fold_difference=2,
    adj_p_value=0.05,
    verbose=False,
):
    if verbose:
        now = datetime.datetime.now()
        print("## START %02d:%02d" % (now.hour, now.minute))

    if out is None:
        out = file_in

    # pd_6dpf_nuclei.write_h5ad('./write/pd_6dpf_nuclei_jake.h5ad')
    if verbose:
        now = datetime.datetime.now()
        print("## READ_FILE %02d:%02d" % (now.hour, now.minute))
    adata = sc.read_h5ad(file_in)

    merge_clusters(
        adata,
        original_clustering,
        num_genes=num_genes,
        min_cells=min_cells,
        fold_difference=fold_difference,
        adj_p_value=adj_p_value,
        verbose=verbose,
    )

    # calculate differential expression for the original clusters
    sc.tl.rank_genes_groups(
        adata,
        groupby=original_clustering,
        key_added=original_clustering,
        method="wilcoxon",
    )
    if verbose:
        now = datetime.datetime.now()
        print("## DEG ORIGINAL %02d:%02d" % (now.hour, now.minute))

    # calculate differential expression for the merged clusters
    merged_clustering = original_clustering + "_merged"
    sc.tl.rank_genes_groups(
        adata, groupby=merged_clustering, key_added=merged_clustering, method="wilcoxon"
    )
    if verbose:
        now = datetime.datetime.now()
        print("## DEG MERGED %02d:%02d" % (now.hour, now.minute))

    # write output
    adata.write_h5ad(out)

    if verbose:
        now = datetime.datetime.now()
        print("## END %02d:%02d" % (now.hour, now.minute))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="""Analyse a high-resolution clustering and merge clusters that don't differ too
        much from each other in terms of differentially expressed genes."""
    )
    parser.add_argument(
        "file_in", metavar="INPUT", help="The clustered object, an AnnData file."
    )
    parser.add_argument(
        "clustering", metavar="CLUSTERING", help="The name of the clustering to merge."
    )
    parser.add_argument(
        "-o",
        "--out",
        dest="file_out",
        metavar="OUTPUT",
        default=None,
        help="""Path to the output object. If not specified, the results will be stored in the .obs
        and .uns slots of the current object.""",
    )
    parser.add_argument(
        "-g",
        "--genes",
        dest="genes",
        default=20,
        help="The cutoff for the DEGs that separate two clusters before merging.",
    )
    parser.add_argument(
        "-c",
        "--cells",
        dest="cells",
        default=10,
        help="The minimum number of cells in a cluster. Smaller clusters will get merged.",
    )
    parser.add_argument(
        "-f",
        "--fold",
        dest="foldchange",
        default=2,
        help="The minimum number of cells in a cluster. Smaller clusters will get merged.",
    )
    parser.add_argument(
        "-p",
        "--pval",
        dest="pval",
        default=0.05,
        help="""The adjusted p-value cutoff below which genes are considered differentially
        expressed.""",
    )
    parser.add_argument(
        "-v",
        "--verbose",
        dest="verbose",
        action="store_true",
        help="Whether to output info during program execution.",
    )

    args = parser.parse_args()
    main(
        file_in=args.file_in,
        original_clustering=args.clustering,
        out=args.file_out,
        num_genes=args.genes,
        min_cells=args.cells,
        fold_difference=args.foldchange,
        verbose=args.verbose,
        adj_p_value=args.pval,
    )