#!/usr/bin/env python

# first mute future warnings and only then import pandas
import warnings
import sys
import yaml

warnings.simplefilter(action="ignore", category=FutureWarning)
from pathlib import Path
import os
import argparse

import numpy as np
from numpy.random import MT19937
from numpy.random import RandomState, SeedSequence
import scipy as sp
from matplotlib import pyplot as plt
import seaborn as sns

import scanpy as sc
import harmonypy as hm

from platython import normalise as pn
from platython import tome as pt


def cluster_small_multiples(
    adata, clust_key, size=60, frameon=False, legend_loc=None, **kwargs
):
    tmp = adata.copy()

    for i, clust in enumerate(adata.obs[clust_key].cat.categories):
        tmp.obs[clust] = adata.obs[clust_key].isin([clust]).astype("category")
        tmp.uns[clust + "_colors"] = ["#d3d3d3", adata.uns[clust_key + "_colors"][i]]

    sc.pl.umap(
        tmp,
        groups=tmp.obs[clust].cat.categories[1:].values,
        color=adata.obs[clust_key].cat.categories.tolist(),
        size=size,
        frameon=frameon,
        legend_loc=legend_loc,
        **kwargs
    )


def main(out_dir, early_info, late_info, verbose=True):
    early_loc, early_cluster, early_name = early_info
    late_loc, late_cluster, late_name = late_info

    # print(early_loc, early_cluster)
    # sys.exit(1)

    # go to out_dir and operate there; create if it doesn't exist
    # thanks to https://stackoverflow.com/a/273227
    Path(out_dir).mkdir(parents=True, exist_ok=True)
    os.chdir(out_dir)

    sc.settings.verbosity = 3
    sc.settings.set_figure_params(dpi=80, facecolor="white")

    rs = RandomState(MT19937(SeedSequence(42)))  # truly random

    # read data
    early = sc.read(early_loc)
    late = sc.read(late_loc)

    # create merged object
    together = early.concatenate(
        late, batch_key="dev_time", batch_categories=[early_name, late_name]
    )
    together.X = together.layers["counts"]

    together.obs["size_factors"] = pn.size_factors(together)

    if verbose:
        sc.pl.scatter(together, "size_factors", "n_counts", save="_n_counts.pdf")
        sc.pl.scatter(together, "size_factors", "n_genes", save="_n_genes")

        fig, ax = plt.subplots()
        sns.distplot(together.obs.size_factors, bins=50, kde=False, ax=ax)
        plt.savefig("figures/size_factors.pdf")

    # Keep the count data in a counts layer
    together.layers["counts"] = together.X.copy()

    # Normalize adata
    together.X /= together.obs["size_factors"].values[:, None]
    # immediately convert to csr else we have a matrix object at tidy.raw.X later
    together.X = sp.sparse.csr_matrix(together.X)
    sc.pp.log1p(together)

    sc.pp.highly_variable_genes(together, min_mean=0.0125, max_mean=3, min_disp=0.5)

    # Run PCA with a standard 50 components; we will only use 20
    # for the UMAP that shows us how the data is not on top of each other.
    sc.pp.pca(together, n_comps=50, use_highly_variable=True, svd_solver="arpack")

    if verbose:
        sc.pp.neighbors(together, n_pcs=20, metric="cosine")
        sc.tl.umap(together, min_dist=0.01)
        sc.pl.umap(
            together, color="dev_time"
        )  # we need this so the "dev_time_colors" slot is initialized
        cluster_small_multiples(
            together, "dev_time", size=10, save="_no_integration.pdf"
        )

    # now integrate using harmony
    ho = hm.run_harmony(
        together.obsm["X_pca"], together.obs, "dev_time", plot_convergence=True
    )
    # save embedding under the appropriate slot
    together.obsm["X_harmony"] = ho.Z_corr.T
    # now calculate neighbors and a low-dimensional embedding,
    # if the users wish it
    if verbose:
        sc.pp.neighbors(together, metric="cosine", use_rep="X_harmony")
        sc.tl.umap(together, min_dist=0.01)
        cluster_small_multiples(together, "dev_time", size=10, save="_integrated.pdf")

    # calculate the distance matrix on the harmony embedding
    d_harm_cos = sp.spatial.distance.pdist(together.obsm["X_harmony"], metric="cosine")
    d_harm_cos = sp.spatial.distance.squareform(d_harm_cos)

    # find the pseudo-ancestors
    res_harm_cos = pt.pseudo_ancestors(
        together,
        d_harm_cos,
        np.array(early.obs[early_cluster], dtype=str),
        np.array(late.obs[late_cluster], dtype=str),
        batch="dev_time",
        bootstrap_reps=10,
        batches=[early_name, late_name],
    )
    if verbose:
        fig, ax = plt.subplots()
        sns.heatmap(res_harm_cos, ax=ax)
        plt.savefig("figures/heatmap.pdf")
    res_harm_cos.to_csv("./result.csv")
    np.savetxt("./dist.csv", d_harm_cos, delimiter=",") 
    together.write_h5ad(f"./pdum{early_name}_{late_name}.h5ad")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Map clusters between consecutive time points."
    )
    parser.add_argument(
        "-o",
        dest="out_dir",
        metavar="output_dir/",
        type=str,
        nargs=1,
        help="location of the output",
        default=None,
    )
    parser.add_argument(
        "--config",
        dest="config",
        metavar="config file",
        type=str,
        nargs=1,
        help="config file instead of specifying seven parameters",
        default=None,
    )
    parser.add_argument("--verbose", dest="verbose", action="store_true", default=False)

    args = parser.parse_args()
    if args.config[0] is not None:
        with open(args.config[0], "r") as ymlfile:
            cfg = yaml.safe_load(ymlfile)
            out_dir = cfg["output_dir"]
            verbose = cfg["verbose"]
            early_info = [
                cfg["early"]["loc"],
                cfg["early"]["clust"],
                cfg["early"]["name"],
            ]
            late_info = [cfg["late"]["loc"], cfg["late"]["clust"], cfg["late"]["name"]]
    main(out_dir, early_info, late_info, verbose)