#!/bin/bash -ex

SCRIPT="/g/arendt/npapadop/repos/platython/scripts/cluster_stability_worker.sh"

INPUT="/g/arendt/npapadop/repos/platython-notebook/48hpf/pd48hpf_good_pdum-v2.1.1.h5ad"
OUTPUT="/g/arendt/npapadop/data/single_cell/10x/48hpf/"

for NCOMPS in 10 20 30 40 50 75; do
    for RES in 0.3 0.7 1.0 1.3 1.7 2.0; do
        for K in 5 10 15 20 25 30; do
            sbatch ${SCRIPT} ${INPUT} ${OUTPUT} ${NCOMPS} ${K} ${RES}
        done
    done
done
