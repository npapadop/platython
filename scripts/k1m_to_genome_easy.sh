#!/bin/bash -ex
#SBATCH -J k1m2draft
#SBATCH -t 1:00:00
#SBATCH -c 1
#SBATCH --mem=2G
#SBATCH -o /g/arendt/npapadop/cluster/k1m2draft_easy_%j.out
#SBATCH -e /g/arendt/npapadop/cluster/k1m2draft_easy_%j.err

# needed file list
SCRIPT="/g/arendt/npapadop/repos/platython/scripts/k1m_to_genome_easy.py"

module load Anaconda3/2020.07
CONDA_BASE=$(conda info --base)
OUTPUT="/g/arendt/npapadop/data/single_cell/10x/k1m_to_draft/"

source ${CONDA_BASE}/etc/profile.d/conda.sh
conda activate /g/arendt/npapadop/repos/condas/scanpy

python ${SCRIPT} --from "$from" --to "$to" -o ${OUTPUT}

conda deactivate /g/arendt/npapadop/repos/condas/scanpy
module unload Anaconda3/2020.07