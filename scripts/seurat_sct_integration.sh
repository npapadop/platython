#!/bin/bash -ex
#SBATCH -J seurat_sct
#SBATCH -t 48:00:00
#SBATCH -N 1
#SBATCH -n 8
#SBATCH --mem=200G
#SBATCH -o /g/arendt/npapadop/cluster/seurat_sct_%j.out
#SBATCH -e /g/arendt/npapadop/cluster/seurat_sct_%j.err
#SBATCH --mail-type=FAIL,END
#SBATCH --mail-user=nikolaos.papadopoulos@embl.de

/g/funcgen/bin/Rscript-4.0.0 /g/arendt/npapadop/repos/pdum_analysis/seurat_integr_cluster.R