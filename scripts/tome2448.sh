#!/bin/bash -ex
#SBATCH -J TOME24-48
#SBATCH -t 0:60:00
#SBATCH -c 1
#SBATCH --mem=50G
#SBATCH -o /g/arendt/npapadop/cluster/TOME_%j.out
#SBATCH -e /g/arendt/npapadop/cluster/TOME_%j.err

# needed file list
SCRIPT="/g/arendt/npapadop/repos/platython/scripts/tome.py"

module load Anaconda3/2020.07
CONDA_BASE=$(conda info --base)
OUTPUT="/g/arendt/npapadop/data/single_cell/10x/TOME/pdum_24hpf_48hpf/"
EARLY="/g/arendt/npapadop/repos/platython-notebook/24hpf/pd24hpf_nuclei_pdum-v2.0.h5ad"
LATE="/g/arendt/npapadop/repos/platython-notebook/48hpf/pd48hpf_cell_nuclei_pdum-v2.0.h5ad"

source ${CONDA_BASE}/etc/profile.d/conda.sh
conda activate /g/arendt/npapadop/repos/condas/scanpy
which python
python ${SCRIPT} -o ${OUTPUT} --from ${EARLY} --to ${LATE} --early-clust leiden --late-clust leiden --early-name 24hpf --late-name 48hpf --verbose

conda deactivate
module unload Anaconda3/2020.07
