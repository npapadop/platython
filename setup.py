#!/usr/bin/env python

from distutils.core import setup

setup(
    name="platython",
    version="0.3",
    description="Platynereis python utilities",
    author="Nikolaos Papadopoulos",
    author_email="npapadosci@gmail.com",
    packages=["platython"],
)
