import numpy as np
import pandas as pd


def grouped_obs_occ(adata, group_key, layer=None):
    if layer is not None:
        getX = lambda x: x.layers[layer]
    else:
        getX = lambda x: x.X

    grouped = adata.obs.groupby(group_key)
    out = pd.DataFrame(
        np.zeros((adata.shape[1], len(grouped)), dtype=np.float64),
        columns=list(grouped.groups.keys()),
        index=adata.var_names,
    )

    for group, idx in grouped.indices.items():
        X = getX(adata[idx])
        out[group] = np.ravel((X > 0).mean(axis=0, dtype=np.float64))
    return out


def exclusivity(percent_expr, in_cutoff=0.8, out_cutoff=0.1):
    exclusive = pd.DataFrame(
        data=False, index=percent_expr.index, columns=percent_expr.columns
    )
    for cluster, perc_gene in percent_expr.iteritems():
        exclusive[cluster] = (perc_gene > in_cutoff) & (
            np.all(percent_expr.drop(cluster, axis=1) < out_cutoff, axis=1)
        )
    return exclusive
