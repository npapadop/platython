import datetime
import numpy as np
import pandas as pd
import matplotlib as mpl
from matplotlib import cm

import scanpy as sc


def timestamp(detail=2, text=""):
    """
    Prints a timestamp with optional text next to it.

    Parameters
    ----------
    detail : int (default: 2)
        The level of detail. 1 for date, 2 for date and time.
    text : str (default: "")
        Extra text to print on the right of the date.
    """
    now = datetime.datetime.now()
    if detail == 1:
        print("## %d-%02d-%02d %s" % (now.year, now.month, now.day, text))
    elif detail == 2:
        print(
            "## %d-%02d-%02d, %02d:%02d %s"
            % (now.year, now.month, now.day, now.hour, now.minute, text)
        )
    else:
        pass


def map_array_to_color(x, palette, xmax=None):
    if xmax is None:
        xmax = np.max(x)
    color = palette(x / xmax)
    return color


def map_to_colormap(x, cmap="magma_r", vmin=0, vmax=None):
    if vmax is None:
        vmax = np.max(x)
    norm = mpl.colors.Normalize(vmin=vmin, vmax=vmax)
    m = cm.ScalarMappable(norm=norm, cmap=cmap)
    return m.to_rgba(x)


def get_connections_from_phylome(
    orthologs, query_genes, target_adata, target_genes=None, show_all=False
):
    if show_all:
        # this is a hack but kinda elegant?
        target_genes = []
    allowed = np.intersect1d(orthologs.index, query_genes)

    found_orthologs = orthologs.loc[allowed].reset_index()
    keep = found_orthologs["orthologs"].isin(target_adata.var.index)
    found_orthologs = found_orthologs[keep]

    connections = np.array(found_orthologs)
    if target_genes is not None:
        unconnected = []
        for gene in query_genes:
            if gene not in connections:
                unconnected.append([gene, None])
        for gene in target_genes:
            if gene not in connections:
                unconnected.append([None, gene])
        connections = np.concatenate((connections, unconnected))
    return connections


def unique_genes(connections):
    real = connections != np.array(None)
    return np.unique(connections[real])


def get_species_orthologs(phylome_loc, species, query_prefix, target_prefix):
    phylome = pd.read_csv(phylome_loc, skiprows=13, sep="\t", header=None).drop(
        columns=[12]
    )
    phylome.columns = [
        "seed",
        "type",
        "target_taxid",
        "orthologs",
        "treefile",
        "source",
        "FamName",
        "PrefName_seed",
        "GeneName_seed",
        "GeneName_target",
        "OG_euk",
        "target_species",
    ]

    keep = phylome["target_species"] == species
    phylome = phylome[keep].copy()
    phylome["gene_id"] = (
        query_prefix
        + "_"
        + phylome["seed"].str.split("\.").str[1].str.split("_").str[:2].str.join("_")
    )
    phylome["orthologs"] = phylome["orthologs"].str.split(",|\|")
    phylome = phylome.explode(column="orthologs")[["gene_id", "orthologs"]].copy()
    phylome["orthologs"] = (
        target_prefix + "_" + phylome["orthologs"].str.split("\.").str[1]
    )

    orthologs = phylome.set_index("gene_id")
    orthologs.drop_duplicates(inplace=True)
    return orthologs


def highlight_cluster(clusters, cluster, bg="black", hl="red"):
    clust_col = [bg] * len(clusters)
    if cluster is None or cluster == "":
        print("Invalid cluster name; returning background color.")
        return clust_col
    if cluster not in clusters:
        print("Cluster not found; returning background color.")
        return clust_col
    highlight = np.where(clusters == cluster)[0][0]
    clust_col[highlight] = hl
    return clust_col


# from ScanPy
def check_colornorm(vmin=None, vmax=None, vcenter=None, norm=None):
    from matplotlib.colors import Normalize

    try:
        from matplotlib.colors import TwoSlopeNorm as DivNorm
    except ImportError:
        # matplotlib<3.2
        from matplotlib.colors import DivergingNorm as DivNorm

    if norm is not None:
        if (vmin is not None) or (vmax is not None) or (vcenter is not None):
            raise ValueError("Passing both norm and vmin/vmax/vcenter is not allowed.")
    else:
        if vcenter is not None:
            norm = DivNorm(vmin=vmin, vmax=vmax, vcenter=vcenter)
        else:
            norm = Normalize(vmin=vmin, vmax=vmax)

    return norm


def pad_string(x, pad_width, padder=" ", side="right"):
    if len(x) < pad_width:
        to_pad = pad_width - len(x)
        pad = "".join([padder] * to_pad)
        if side == "right":
            x = x + pad
        elif side == "left":
            x = pad + x
        else:
            print("Invalid side argument; returning string as-is.")
    return x


def collapse_unrelated_clusters(adata, cluster, fine, coarse):
    cluster_members = adata.obs[fine] == cluster
    major_cluster_category = adata.obs[coarse][cluster_members].value_counts().index[0]

    major_category_members = np.array(adata.obs[coarse] == major_cluster_category)

    summary = adata.obs[coarse].astype(str).values
    summary[major_category_members] = adata.obs[fine][major_category_members].values
    adata.obs[fine + "_collapsed"] = summary
    adata.obs[fine + "_collapsed"] = adata.obs[fine + "_collapsed"].astype("category")


def get_orthologs(genes, orthology, target, celltype_to):
    genes_in_table = np.intersect1d(genes, orthology.index)

    to_include = np.sum(orthology.loc[genes_in_table]) == 2
    orthologs = np.intersect1d(orthology.columns[to_include], target.var.index)

    to_include = np.sum(orthology.loc[genes_in_table]) == 1
    paralogs = np.intersect1d(orthology.columns[to_include], target.var.index)

    scores = pd.DataFrame(target.uns["rank_genes_groups"]["scores"])[celltype_to]
    names = pd.DataFrame(target.uns["rank_genes_groups"]["names"])[celltype_to]

    framed = pd.DataFrame(scores).set_index(names)
    significant = framed.loc[paralogs] > 0.01
    homologs = np.concatenate((orthologs, paralogs[significant[celltype_to]]))

    subset = orthology.loc[genes_in_table][homologs].melt(ignore_index=False)
    subset.reset_index(drop=False, inplace=True)

    connections = np.array(subset[subset["value"] > 0])

    not_in_table = np.setdiff1d(genes, orthology.index)
    if not_in_table.size > 0:
        unconnected = np.array([[g, None, 0] for g in not_in_table])
        return np.concatenate((connections, unconnected))
    else:
        return connections


def get_orthologs_overlap(genes1, genes2, query, target, orthology):
    """
    Returns a DataFrame of homologous gene pairs between two sets of genes based on their presence
    in an orthology table.

    Parameters
    ----------
    genes1 : numpy.ndarray
        A series of gene names.
    genes2 : numpy.ndarray
        A series of gene names.
    query : anndata.AnnData
        An AnnData object containing the query genes as indices of the `.var` slot.
    target : anndata.AnnData
        An AnnData object containing the target genes as indices of the `.var` slot.
    orthology : pandas.core.frame.DataFrame
        A DataFrame containing the orthology information.

    Returns
    -------
    connections : pandas.core.frame.DataFrame
        A DataFrame of homologous gene pairs and their degree of conservation. The array has
        three columns: 'query', 'target', and 'degree', where 'query' and 'target' are the gene
        names, and 'degree' is the degree of conservation, which can be either 1 or 2.
    """
    genes1_in_data = np.intersect1d(genes1, query.var.index)
    genes2_in_data = np.intersect1d(genes2, target.var.index)
    genes1_in_table = np.intersect1d(genes1_in_data, orthology.index)
    genes2_in_table = np.intersect1d(genes2_in_data, orthology.columns)

    connections = []

    subset = orthology.loc[genes1_in_table][genes2_in_table]
    _qo, _to = np.where(subset == 2)
    for q, t in zip(subset.index[_qo], subset.columns[_to]):
        connections.append([q, t, "2"])

    _qo, _to = np.where(subset == 1)
    for q, t in zip(subset.index[_qo], subset.columns[_to]):
        connections.append([q, t, "1"])

    return np.array(connections)


def cell_cycle_score(
    adata,
    g2m_names="TOP2A|TPX2|SMC4|AURKA|NCAPD2|KIF11|KIF23|CENPE|CENPF|CKAP2L|NUF2|CDK1|DLGAP5|HMMR|G2E3|CTCF|BIRC5",
    s_names="FEN1|MSH2|UNG|CDC45|BRIP1|TYMS|POLA1|E2F8|DTL|MCM2|MCM6|DSCC1|CLSPN|RAD51|GINS2|EXO1|RRM1",
    desc="description",
    gene_name="emapper_name",
    gene_id="gene_ids",
):
    adata.var[desc].fillna("-", inplace=True)
    adata.var[gene_name].fillna("-", inplace=True)
    g2m = adata.var[adata.var[gene_name].str.contains(g2m_names, case=False)][
        gene_id
    ].values
    s = adata.var[adata.var[gene_name].str.contains(s_names, case=False)][
        gene_id
    ].values
    sc.tl.score_genes_cell_cycle(adata, s, g2m)
