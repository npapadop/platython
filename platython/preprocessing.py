import pandas as pd
import numpy as np


def get_mtdna(rencken_loc, k1m_mito_loc, bonus_mtdna_loc):
    # query Simone's list
    tmp = pd.read_csv(rencken_loc, sep="\t")
    rencken_genes = tmp[["features", "BLAST_result_details"]]
    is_mito = rencken_genes["BLAST_result_details"].str.contains("mito")
    rencken_mito = rencken_genes["features"][is_mito]

    # additional mito entries from aligning to the mitochondrial genome
    k1m_mito_list = pd.read_csv(k1m_mito_loc, sep="\t")
    k1m_mito_candidates = (
        k1m_mito_list["Name"].str.replace("TRINITY_", "").str.replace("_i.*", "")
    )

    # bonus entries from the mmseqs2 annotation
    bonus_mtdna_list = pd.read_csv(bonus_mtdna_loc, sep="\t")
    bonus_mtdna_candidates = bonus_mtdna_list["gene"].str.replace("-", "_")

    # only keep unique IDs
    return np.unique(
        np.concatenate((rencken_mito, k1m_mito_candidates, bonus_mtdna_candidates))
    )


def get_rrna(rencken_loc):
    tmp = pd.read_csv(rencken_loc, sep="\t")
    rencken_genes = tmp[["features", "BLAST_result_details"]]
    is_ribo = rencken_genes["BLAST_result_details"].str.contains("RNA")
    return np.unique(rencken_genes["features"][is_ribo])


def add_percent_mtdna(adata, mito):
    # find which genes in the object are mitochondrial and add them as a slot
    _overlap, mito_genes, _mito_expressed = np.intersect1d(
        adata.var.index, mito, return_indices=True
    )
    is_mito = np.zeros(len(adata.var.index), dtype=bool)
    is_mito[mito_genes] = True
    adata.var["mito"] = is_mito
    # go over each cell and sum up the non-mtdna percent of reads
    umis_per_cell = np.asarray(np.sum(adata.X, axis=1)).flatten()
    mito_per_cell = np.asarray(np.sum(adata.X[:, adata.var["mito"]], axis=1)).flatten()
    percent_mtdna = mito_per_cell / umis_per_cell
    adata.obs["mtdna"] = percent_mtdna


def add_percent_rrna(adata, ribo):
    # find which genes in the object are ribosomal and add them as a slot
    _overlap, ribo_genes, _ribo_expressed = np.intersect1d(
        adata.var.index, ribo, return_indices=True
    )
    is_ribo = np.zeros(len(adata.var.index), dtype=bool)
    is_ribo[ribo_genes] = True
    adata.var["ribo"] = is_ribo
    # go over each cell and sum up the non-rrna percent of reads
    umis_per_cell = np.asarray(np.sum(adata.X, axis=1)).flatten()
    ribo_per_cell = np.asarray(np.sum(adata.X[:, adata.var["ribo"]], axis=1)).flatten()
    percent_rrna = ribo_per_cell / umis_per_cell
    adata.obs["rrna"] = percent_rrna


def add_percent_good_genes(adata):
    # mark non-ribo/mtdna genes as "good"
    adata.var["good_genes"] = ~(adata.var["mito"] | adata.var["ribo"])

    # now use the %ribo and %mito to calculate %good gene for each cell
    perc_ribo = 1 - adata.obs["rrna"]
    perc_mito = 1 - adata.obs["mtdna"]
    adata.obs["good_genes"] = (perc_ribo + perc_mito) - 1


def get_tidy_object(adata, mito_cutoff=0.25, ribo_cutoff=0.0):
    """
    Subset objects to only include cells with low mitochondrial content and genes that are not
    mitochondrial or ribosomal.
    """
    good_cells = (adata.obs["mtdna"] < mito_cutoff) & (adata.obs["rrna"] < ribo_cutoff)
    return adata[good_cells][:, adata.var["good_genes"]]


def add_count_stats(adata):
    """
    Count total UMIs and total genes expressed in a cell.
    """
    adata.obs["nCount_RNA"] = np.asarray(np.sum(adata.X, axis=1))[:, 0]
    adata.obs["nFeature_RNA"] = np.asarray(np.sum(adata.X > 0, axis=1))[:, 0]


def get_LUT(lut_loc):
    """
    Get the look-up table with the emapper and MMSeqs2 annotation.
    """
    LUT = pd.read_csv(lut_loc, sep="\t")
    LUT["gene"] = LUT["gene"].str.replace("-", "_")
    LUT = LUT.set_index("gene")
    LUT.rename(
        columns={
            "gene": "gene_ids",
            "Preferred_name": "emapper_name",
            "ref_names": "human_name",
            "Description": "description",
        },
        inplace=True,
    )
    return LUT


def add_annotation(adata, lut_loc):
    LUT = get_LUT(lut_loc)
    adata.var = adata.var.join(LUT)
    isnan = adata.var["emapper_name"].isnull()
    missing = adata.var["emapper_name"] == "-"

    no_desc = adata.var["description"].isnull()
    desc_mis = adata.var["description"] == "-"

    has_name = ~isnan & ~missing
    has_desc = ~no_desc & ~desc_mis

    named = adata.var["gene_ids"][has_name].str.cat(
        adata.var["emapper_name"][has_name], sep="|"
    )

    described_not_named = adata.var["gene_ids"][~has_name & has_desc].str.cat(
        adata.var["description"][~has_name & has_desc], sep="|"
    )

    unnamed = adata.var["gene_ids"][~has_name & ~has_desc]

    uniquely_named = pd.concat([named, described_not_named, unnamed])
    uniquely_named = pd.DataFrame(uniquely_named)
    uniquely_named.rename(columns={"gene_ids": "name"}, inplace=True)

    adata.var = adata.var.join(uniquely_named)


def add_manual_annotation(adata, google_docs):
    markers = pd.read_csv(google_docs)
    markers = markers[~markers["pdum-v2.1"].isna()]

    in_dataset = markers["pdum-v2.1"].isin(adata.var.index)
    markers = markers[in_dataset]
    emapper = (
        adata.var.loc[markers["pdum-v2.1"]]["name"]
        .astype(str)
        .replace(np.NaN, "")
        .values
    )
    markers["annot"] = emapper + " (" + markers["gene"] + ")"

    adata.var["annot"] = adata.var["name"].astype(str).replace(np.NaN, "")
    tmp = markers[["pdum-v2.1", "annot"]]
    tmp.set_index("pdum-v2.1", inplace=True)
    tmp = tmp[~tmp.index.duplicated()]

    adata.var.update(tmp)


def add_mito_ribo(adata):
    mitochondrial = adata.var["gene_ids"].str.startswith("gene-")
    adata.var["mito"] = mitochondrial
    ribosomal = adata.var["gene_ids"].str.startswith("rna-")
    adata.var["ribo"] = ribosomal

    adata.obs["mtdna"] = np.sum(adata.X[:, mitochondrial], axis=1) / np.sum(
        adata.X, axis=1
    )
    adata.obs["rrna"] = np.sum(adata.X[:, ribosomal], axis=1) / np.sum(adata.X, axis=1)
