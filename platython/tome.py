import numpy as np
from tqdm import tqdm
import pandas as pd


def pseudo_ancestors(
    adata,
    dist,
    early_clusters,
    late_clusters,
    subsample_frac=0.8,
    bootstrap_reps=500,
    nearest_neighbors=5,
    batch="batch",
    batches=["early", "late"],
    verbose=True,
):
    # count the clusters in the early and late object and define the result array
    no_early_clusters = len(np.unique(early_clusters))
    order_of = {n: i for i, n in enumerate(np.unique(early_clusters))}
    no_late_clusters = len(np.unique(late_clusters))
    results = np.zeros((bootstrap_reps, no_late_clusters, no_early_clusters))

    # concatenate the cluster assignments
    all_clusters = np.concatenate((early_clusters, late_clusters))

    # calculate how many cells are `subsample_frac` of the total
    N = adata.shape[0]
    fraction = int(subsample_frac * N)

    if verbose:
        loop = tqdm(range(bootstrap_reps))
    else:
        loop = range(bootstrap_reps)
    for i in loop:
        keep = np.random.choice(N, fraction, replace=False)
        # separate the indices that belong to the early/late batch
        early_inds = keep[adata.obs.iloc[keep][batch] == batches[0]]
        late_inds = keep[adata.obs.iloc[keep][batch] == batches[1]]
        # subset the distance matrix and sort it
        closest_neighbors = np.argsort(dist[late_inds][:, early_inds], axis=1)
        # only keep the closest neighbors
        nn = closest_neighbors[:, :nearest_neighbors]
        # figure out which cluster assignments belong to the subset:
        cluster_early = all_clusters[early_inds[nn]]
        cluster_late = all_clusters[late_inds]

        for j, cl in enumerate(np.unique(late_clusters)):
            names, freq = np.unique(
                cluster_early[cluster_late == cl], return_counts=True
            )
            for k, name in enumerate(names):
                results[i, j, order_of[name]] = freq[k] / np.sum(freq)

    res = pd.DataFrame(
        data=np.median(results, axis=0),
        columns=np.unique(early_clusters),
        index=np.unique(late_clusters),
    )
    return res
