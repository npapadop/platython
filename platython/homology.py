from tqdm import tqdm

import numpy as np
import pandas as pd
import scipy as sp

import anndata as ad

def _keep_level(OGs, level="Metazoa"):
    """
    Filter a list of orthogroups by a specific level. Designed to be used
    by an `.apply` method on a Pandas dataframe.

    Parameters
    ----------
    OGs : list
        A list of orthogroups as produced by eggNOG-mapper.
    level : str
        The level to filter by. Default is "Metazoa".

    Returns
    -------
    str
        the orthogroup that contains the selected orthology level.
    """
    for orthogroup in OGs:
        if level in orthogroup:
            return orthogroup
        
def extract_OG_at_level(adata, eggNOG_OGs="eggNOG_OGs", level="Metazoa"):
    """
    Extract orthogroups at a specific level from a DataFrame and add that as a new column.

    Parameters
    ----------
    adata : anndata.AnnData
        An AnnData object containing orthogroups as columns.
    level : str
        The level to filter by. Default is "Metazoa".

    Returns
    -------
    None
    """
    adata.var[level.lower()+"_og"] = adata.var[eggNOG_OGs].fillna("-").str.split(",").apply(lambda x: _keep_level(x, level=level))

def collapse_by_OG(adata, eggnog_OGs="eggNOG_OGs", level="Metazoa"):
    """
    Collapse the expression matrix into orthogroups. For all genes that get an orthogroup assignment
    at the desired level, sum up the expression values per orthogroup per cell. This will be saved
    in the "orthogroups" slot in `.obsm`, but it is recommended to create a new AnnData object with
    this as `.X`. Orthogroup names are saved in `.uns` as a list under `"{level}_og"`.

    Parameters
    ----------
    adata : AnnData object
        The AnnData object containing the expression matrix and orthogroup information.
    eggnog_OGs : str, optional
        The `.var` slot that holds the orthogroup information, by default "eggNOG_OGs".
    level : str, optional
        The orthology level to use for the collapsing, by default "Metazoa". It is recommended to
        use the most specific level available.
    """
    extract_OG_at_level(adata, eggNOG_OGs=eggnog_OGs, level=level)
    N = adata.shape[0]
    # G is the number of unique orthogroups
    real = adata.var[level.lower()+"_og"].isna() == False
    unique_orthogroups = adata.var[level.lower()+"_og"][real].unique()
    G = len(unique_orthogroups)
    # create a new matrix to hold the orthogroup expression
    orthogroups = np.zeros((N, G))
    # go through all the orthogroups and sum up the corresponding rows of the expression matrix
    for i, OG in enumerate(tqdm(unique_orthogroups)):
        tmp = adata[:, adata.var[level.lower()+"_og"] == OG].X.sum(axis=1)
        orthogroups[:, i] = np.array(tmp)[:, 0]
    orthogroups = sp.sparse.csr_matrix(orthogroups)
    # save the OG expression in the AnnData object
    adata.obsm["orthogroups"] = orthogroups
    adata.uns[level.lower()+"_og"] = unique_orthogroups