from tqdm import tqdm

import matplotlib.pyplot as plt
from matplotlib import colors
import matplotlib.cm as cm
import matplotlib.patches as mpatches
from matplotlib.patches import Patch
from matplotlib.patches import Rectangle
from matplotlib.patches import Circle
from matplotlib.lines import Line2D
from matplotlib.colors import LinearSegmentedColormap
from mpl_toolkits.axes_grid1 import make_axes_locatable

import seaborn as sns
import pandas as pd
from pandas.api.types import is_categorical_dtype

import numpy as np
from scipy.stats import gaussian_kde

import scanpy as sc
from scanpy.plotting import _utils
from typing import Tuple
from functools import partial

# from descartes import PolygonPatch
# import alphashape

from platython import util as pu
from platython import cluster as pc


godsnot_102 = np.array(
    [
        "#FFFF00",
        "#1CE6FF",
        "#FF34FF",
        "#FF4A46",
        "#008941",
        "#006FA6",
        "#A30059",
        "#FFDBE5",
        "#7A4900",
        "#0000A6",
        "#63FFAC",
        "#B79762",
        "#004D43",
        "#8FB0FF",
        "#997D87",
        "#5A0007",
        "#809693",
        "#6A3A4C",
        "#1B4400",
        "#4FC601",
        "#3B5DFF",
        "#4A3B53",
        "#FF2F80",
        "#61615A",
        "#BA0900",
        "#6B7900",
        "#00C2A0",
        "#FFAA92",
        "#FF90C9",
        "#B903AA",
        "#D16100",
        "#DDEFFF",
        "#000035",
        "#7B4F4B",
        "#A1C299",
        "#300018",
        "#0AA6D8",
        "#013349",
        "#00846F",
        "#372101",
        "#FFB500",
        "#C2FFED",
        "#A079BF",
        "#CC0744",
        "#C0B9B2",
        "#C2FF99",
        "#001E09",
        "#00489C",
        "#6F0062",
        "#0CBD66",
        "#EEC3FF",
        "#456D75",
        "#B77B68",
        "#7A87A1",
        "#788D66",
        "#885578",
        "#FAD09F",
        "#FF8A9A",
        "#D157A0",
        "#BEC459",
        "#456648",
        "#0086ED",
        "#886F4C",
        "#34362D",
        "#B4A8BD",
        "#00A6AA",
        "#452C2C",
        "#636375",
        "#A3C8C9",
        "#FF913F",
        "#938A81",
        "#575329",
        "#00FECF",
        "#B05B6F",
        "#8CD0FF",
        "#3B9700",
        "#04F757",
        "#C8A1A1",
        "#1E6E00",
        "#7900D7",
        "#A77500",
        "#6367A9",
        "#A05837",
        "#6B002C",
        "#772600",
        "#D790FF",
        "#9B9700",
        "#549E79",
        "#FFF69F",
        "#201625",
        "#72418F",
        "#BC23FF",
        "#99ADC0",
        "#3A2465",
        "#922329",
        "#5B4534",
        "#FDE8DC",
        "#404E55",
        "#0089A3",
        "#CB7E98",
        "#A4E804",
        "#324E72",
    ]
)

org_dict = {
    "red": ((0.0, 1.0, 1.0), (0.5, 1.0, 1.0), (1.0, 1.0, 1.0)),
    "green": ((0.0, 1.0, 1.0), (0.5, 0.8, 0.8), (1.0, 0.6, 0.6)),
    "blue": ((0.0, 1.0, 1.0), (0.5, 0.5, 0.5), (1.0, 0.0, 0.0)),
    "alpha": ((0.0, 0.0, 0.0), (0.5, 0.25, 0.25), (1.0, 0.5, 0.5)),
}

oranges = LinearSegmentedColormap("Oranges", org_dict)
# oranges(0.5)

sky_dict = {
    "blue": ((0.0, 1.0, 1.0), (0.5, 1.0, 1.0), (1.0, 1.0, 1.0)),
    "green": ((0.0, 1.0, 1.0), (0.5, 0.7, 0.7), (1.0, 0.4, 0.4)),
    "red": ((0.0, 1.0, 1.0), (0.5, 0.5, 0.5), (1.0, 0.0, 0.0)),
    "alpha": ((0.0, 0.0, 0.0), (0.5, 0.25, 0.25), (1.0, 0.5, 0.5)),
}

sky = LinearSegmentedColormap("Skyblue", sky_dict)
# sky(0.5)


def diagnostics_hists(obj_list, obj_names):
    """
    Shortcut for basic QC plotting. Will output a 2x2 grid with
    - log(rRNA%) histogram
    - log(mtDNA%) histogram
    - log(#UMIs per cell) histogram
    - log(#genes per cell) histogram
    for each `AnnData` object it is passed.

    Parameters
    ----------
    obj_list: list
        A list of `AnnData` objects.
    obj_names: list
        A list with the names of the objects.
    """
    fig, ax = plt.subplots(ncols=2, nrows=2)
    fig.set_size_inches(10, 10)
    for i, obj in enumerate(obj_list):
        ax[0, 0].hist(
            np.log(obj.obs["rrna"] + 0.01),
            alpha=0.5,
            bins=50,
            label=obj_names[i],
            density=True,
        )
        ax[0, 0].set_title("Density plot of log-%rrna")
        ax[0, 0].legend()

        ax[0, 1].hist(
            np.log(obj.obs["mtdna"] + 0.01),
            alpha=0.5,
            bins=50,
            label=obj_names[i],
            density=True,
        )
        ax[0, 1].set_title("Density plot of log(x+0.01) of %mtdna")
        # ax.axvline(np.log(0.1 + 0.01))
        ax[0, 1].legend()

        ax[1, 0].hist(
            np.log(obj.obs["nCount_RNA"]),
            alpha=0.5,
            bins=50,
            label=obj_names[i],
            density=True,
        )
        ax[1, 0].set_title("Density plot of log(nCount_RNA)")
        # ax.axvline(np.log(0.1 + 0.01))
        ax[1, 0].legend()

        ax[1, 1].hist(
            np.log(obj.obs["nFeature_RNA"]),
            alpha=0.5,
            bins=50,
            label=obj_names[i],
            density=True,
        )
        ax[1, 1].set_title("Density plot of log(nFeature_RNA)")
        # ax.axvline(np.log(0.1 + 0.01))
        ax[1, 1].legend()


def diagnostics_scatters(obj_list, obj_names):
    fig, ax = plt.subplots(ncols=2, nrows=2)
    fig.set_size_inches(10, 10)
    for i, obj in enumerate(obj_list):
        sns.kdeplot(
            x=obj.obs["good_genes"],
            y=np.log(obj.obs["nFeature_RNA"]),
            ax=ax[0, 0],
            label=obj_names[i],
        )
        ax[0, 0].set_title("log(#genes) ~ %good genes")
        ax[0, 0].legend()

        sns.kdeplot(
            x=obj.obs["mtdna"],
            y=np.log(obj.obs["nFeature_RNA"]),
            ax=ax[0, 1],
            label=obj_names[i],
        )
        ax[0, 1].set_title("log(#genes) ~ %mtdna")
        # ax.axvline(np.log(0.1 + 0.01))
        ax[0, 1].legend()

        sns.kdeplot(
            x=obj.obs["rrna"],
            y=np.log(obj.obs["nFeature_RNA"]),
            ax=ax[1, 0],
            label=obj_names[i],
        )
        ax[1, 0].set_title("log(#genes) ~ %rrna")
        # ax.axvline(np.log(0.1 + 0.01))
        ax[1, 0].legend()

        ax[1, 1].scatter(
            obj.obs["nCount_RNA"],
            obj.obs["nFeature_RNA"],
            alpha=0.5,
            label=obj_names[i],
        )
        ax[1, 1].set_title("#genes ~ #UMIs")
        # ax.axvline(np.log(0.1 + 0.01))
        ax[1, 1].legend()


def _color_vector(
    adata, values_key: str, values, palette, na_color="lightgray"
) -> Tuple[np.ndarray, bool]:
    """
    (From ScanPy)

    Map array of values to array of hex (plus alpha) codes.
    For categorical data, the return value is list of colors taken
    from the category palette or from the given `palette` value.
    For continuous values, the input array is returned (may change in future).
    """
    ###
    # when plotting, the color of the dots is determined for each plot
    # the data is either categorical or continuous and the data could be in
    # 'obs' or in 'var'
    to_hex = partial(colors.to_hex, keep_alpha=True)
    if values_key is None:
        return np.broadcast_to(to_hex(na_color), adata.n_obs), False
    if not is_categorical_dtype(values):
        return values, False
    else:  # is_categorical_dtype(values)
        color_map = _get_palette(adata, values_key, palette=palette)
        color_vector = values.map(color_map).map(to_hex)

        # Set color to 'missing color' for all missing values
        if color_vector.isna().any():
            color_vector = color_vector.add_categories([to_hex(na_color)])
            color_vector = color_vector.fillna(to_hex(na_color))
        return color_vector, True


def _get_palette(adata, values_key: str, palette=None):
    color_key = f"{values_key}_colors"
    values = pd.Categorical(adata.obs[values_key])
    if palette:
        _utils._set_colors_for_categorical_obs(adata, values_key, palette)
    elif color_key not in adata.uns or len(adata.uns[color_key]) < len(
        values.categories
    ):
        #  set a default palette in case that no colors or few colors are found
        _utils._set_default_colors_for_categorical_obs(adata, values_key)
    else:
        _utils._validate_palette(adata, values_key)
    return dict(zip(values.categories, adata.uns[color_key]))


def _get_color_source_vector(
    adata, value_to_plot, use_raw=False, gene_symbols=None, layer=None, groups=None
):
    """
    (From ScanPy)

    Get array from adata that colors will be based on.
    """
    if value_to_plot is None:
        # Points will be plotted with `na_color`. Ideally this would work
        # with the "bad color" in a color map but that throws a warning. Instead
        # _color_vector handles this.
        # https://github.com/matplotlib/matplotlib/issues/18294
        return np.broadcast_to(np.nan, adata.n_obs)
    if (
        gene_symbols is not None
        and value_to_plot not in adata.obs.columns
        and value_to_plot not in adata.var_names
    ):
        # We should probably just make an index for this, and share it over runs
        value_to_plot = adata.var.index[adata.var[gene_symbols] == value_to_plot][
            0
        ]  # TODO: Throw helpful error if this doesn't work
    if use_raw and value_to_plot not in adata.obs.columns:
        values = adata.raw.obs_vector(value_to_plot)
    else:
        values = adata.obs_vector(value_to_plot, layer=layer)
    if groups and is_categorical_dtype(values):
        values = values.replace(values.categories.difference(groups), np.nan)
    return values


def get_color(adata, value_to_plot, palette):
    color_source_vector = _get_color_source_vector(adata, value_to_plot)

    color_vector, _categorical = _color_vector(
        adata, value_to_plot, color_source_vector, palette=palette
    )

    ### Order points
    order = slice(None)
    order = np.argsort(-color_vector, kind="stable")[::-1]

    color_source_vector = color_source_vector[order]
    color_vector = color_vector[order]
    color_vector = color_vector / np.max(color_vector)
    #     color_vector[:, -1] = color_source_vector / np.max(color_source_vector) * 0.5
    return color_source_vector, color_vector, order


# def plot_coexpression(
#     adata,
#     groupby,
#     colors,
#     labels=None,
#     legend_loc="upper right",
#     highlight_clusters=True,
# ):
#     x = adata.obsm["X_umap"]

#     _fig, ax = plt.subplots(figsize=(15, 10))
#     divider = make_axes_locatable(ax)
#     ax.scatter(x[:, 0], x[:, 1], c="none", s=10)
#     ax.set_xticklabels("")
#     ax.set_yticklabels("")
#     ax.grid(False)
#     cax1 = divider.append_axes("right", size="5%", pad=0.05)
#     cax2 = divider.append_axes("left", size="5%", pad=0.05)

#     for i, cluster in enumerate(adata.obs[groupby].values.categories):
#         edge_color = "black"
#         if highlight_clusters:
#             edge_color = adata.uns[groupby + "_colors"][i]
#         keep = adata.obs[groupby] == cluster
#         alpha_shape = alphashape.alphashape(x[keep], 5)
#         alpha_shape = alpha_shape.buffer(0.1)
#         ax.add_patch(
#             PolygonPatch(alpha_shape, facecolor="none", edgecolor=edge_color, lw=1.0)
#         )

#     geneA, _colorA, orderA = get_color(adata, colors[0], sky)
#     im = ax.scatter(x[orderA, 0], x[orderA, 1], c=geneA, s=40, cmap=sky)
#     im.set_clim(np.min(geneA), np.max(geneA))
#     plt.colorbar(im, cax=cax1, orientation="vertical")
#     geneB, _colorB, orderB = get_color(adata, colors[1], oranges)
#     im = ax.scatter(x[orderB, 0], x[orderB, 1], c=geneB, s=40, cmap=oranges)
#     im.set_clim(np.min(geneB), np.max(geneB))
#     plt.colorbar(im, cax=cax2, orientation="vertical")
#     cax2.yaxis.set_ticks_position("left")

#     geneA = geneA[np.argsort(orderA)]
#     geneB = geneB[np.argsort(orderB)]
#     coexp = (geneA > 0) & (geneB > 0)

#     ax.scatter(x[coexp, 0], x[coexp, 1], c="black", s=15, marker="x")

#     if labels is None:
#         labels = colors
#     legend_elements = [
#         mpatches.Patch(facecolor="skyblue", label=labels[0]),
#         mpatches.Patch(facecolor="orange", label=labels[1]),
#         Line2D(
#             [0],
#             [0],
#             marker="X",
#             color="black",
#             label="co-expression",
#             ls="None",
#             markerfacecolor="black",
#             markersize=10,
#         ),
#     ]
#     ax.legend(handles=legend_elements, loc=legend_loc)

#     plt.show()


def print_dotplot(
    adata,
    marker_names,
    prefix,
    groupby="leiden",
    gene_symbols="emapper",
    results_per_page=80,
):
    markers = np.concatenate(marker_names)
    num_markers = [len(m) for m in marker_names]
    for i in range(np.sum(num_markers) // results_per_page):
        sc.pl.dotplot(
            adata,
            markers[i * results_per_page : (i + 1) * results_per_page],
            gene_symbols=gene_symbols,
            groupby=groupby,
            swap_axes=True,
            save=prefix + f"{(i):02d}" + ".pdf",
        )

    sc.pl.dotplot(
        adata,
        markers[(i + 1) * results_per_page :],
        gene_symbols=gene_symbols,
        groupby=groupby,
        swap_axes=True,
        save=prefix + f"{(i+1):02d}" + ".pdf",
    )

# a function to pick the genes that have a name in the top X differentially expressed genes
def named_genes_clust(adata, cluster, top=500, var_name="annot", slot="rank_genes_groups"):
    top_genes = pd.DataFrame(adata.uns[slot]["names"]).loc[:, cluster][
        :top
    ]
    return adata.var.loc[top_genes][
        adata.var.loc[top_genes][var_name].str.contains("\||\(")
    ][var_name]


def cluster_small_multiples(
    adata, clust_key, size=60, frameon=False, legend_loc=None, **kwargs
):
    tmp = adata.copy()

    for i, clust in enumerate(adata.obs[clust_key].cat.categories):
        tmp.obs[clust] = adata.obs[clust_key].isin([clust]).astype("category")
        tmp.uns[clust + "_colors"] = ["#d3d3d3", adata.uns[clust_key + "_colors"][i]]

    sc.pl.umap(
        tmp,
        groups=tmp.obs[clust].cat.categories[1:].values,
        color=adata.obs[clust_key].cat.categories.tolist(),
        size=size,
        frameon=frameon,
        legend_loc=legend_loc,
        **kwargs,
    )


def plot_cluster_composition(adata, cluster, batch, batch_labels=None, figsize=None):
    count_series = adata.obs.groupby([cluster, batch]).size()
    new_df = count_series.to_frame(name="size").reset_index()
    constitution = new_df.pivot(index=cluster, columns=batch)["size"]

    perc_clust = np.array((constitution.T / np.sum(constitution.T, axis=0)))

    clusters = adata.obs[cluster].cat.categories
    batches = adata.obs[batch].cat.categories
    if figsize is None:
        figsize = (len(clusters), 5)
    if batch_labels is None:
        batch_labels = batches
    fig, ax = plt.subplots(figsize=figsize)
    ax.grid(False)
    bottom = np.zeros(clusters.shape)
    for i, b in enumerate(batches):
        ax.bar(
            clusters, perc_clust[i], 0.6, bottom=bottom, yerr=0, label=batch_labels[i]
        )
        bottom += perc_clust[i]

    ax.legend(loc="right", bbox_to_anchor=(1.2, 0.5))


def domino_plot(
    adata,
    gene,
    layer=None,
    palette=cm.magma_r,
    group_by="leiden",
    title="annot",
    width=0.6,
    log=True,
    figsize=(15, 5),
):
    avg_exp = pc.grouped_obs_mean(adata, group_by, layer=layer)
    avg_exp_color = pu.map_array_to_color(avg_exp.loc[gene], palette)
    cluster_color = {cluster: avg_exp_color[i] for i, cluster in enumerate(avg_exp.columns)}

    perc_exp = pc.grouped_obs_present(adata, group_by)

    group_by_categories, samples_per_category = np.unique(
        adata.obs[group_by], return_counts=True
    )
    order = np.argsort(-samples_per_category)
    group_by_categories = group_by_categories[order]

    samples_per_category = samples_per_category[order]
    perc_per_category = perc_exp.loc[gene]
    if log:
        samples_per_category = np.log2(samples_per_category + 1)
        perc_per_category = np.log2(perc_per_category + 1)

    N = len(group_by_categories)
    grid = np.mgrid[0:N:1, 0:1:1].reshape(2, -1).T

    fig, ax = plt.subplots(figsize=figsize)
    for i in range(N):
        cluster = group_by_categories[i]
        outline = mpatches.Rectangle(
            grid[i] - [width / 2, 0],
            width,
            samples_per_category[i],
            edgecolor="black",
            facecolor="white",
        )
        express = mpatches.Rectangle(
            grid[i] - [width / 2, 0],
            width,
            perc_per_category[cluster],
            edgecolor="black",
            facecolor=cluster_color[cluster],
        )
        ax.add_patch(outline)
        ax.add_patch(express)

    ax.set_xlim(-1, N)
    ax.set_ylim(0, np.max(samples_per_category) + np.min(samples_per_category))
    ax.set_xticks(np.arange(N))
    ax.set_xticklabels(group_by_categories)
    ax.grid(False, axis="x")
    ax.set_title(adata.var[title].loc[gene])

    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="4%", pad=0.5)

    vmin = min(0, np.min(avg_exp.loc[gene]))
    vmax = np.max(avg_exp.loc[gene])
    sm = plt.cm.ScalarMappable(cmap=palette, norm=plt.Normalize(vmin=vmin, vmax=vmax))
    sm._A = []
    plt.colorbar(sm, cax=cax)


def map_fine_to_coarse(
    sm, species, fine, coarse, plot=sc.pl.umap, include_coarse=False
):
    fine_to_coarse = (
        sm.sams[species]
        .adata.obs[[fine, coarse]]
        .drop_duplicates()
        .reset_index(drop=True)
    )

    plot(sm.sams[species].adata, color=coarse)

    lut = dict(
        zip(
            sm.sams[species].adata.obs[coarse].cat.categories,
            sm.sams[species].adata.uns[coarse + "_colors"],
        )
    )
    handles = [Patch(facecolor=lut[name]) for name in lut]
    if include_coarse:
        fine_to_coarse[fine] = (
            species
            + "_"
            + fine_to_coarse[coarse].astype(str)
            + "_"
            + fine_to_coarse[fine].astype(str)
        )
    else:
        fine_to_coarse[fine] = species + "_" + fine_to_coarse[fine].astype(str)
    return fine_to_coarse, lut, handles


def plot_overview(to_plot, celltype_from, celltype_to, save=None):
    figwidth = int(to_plot.shape[0] / to_plot.shape[1] * 20)
    figheight = 20
    y = np.where(to_plot.columns == celltype_from)[0][0]
    x = np.where(to_plot.index == celltype_to)[0][0]

    fig, ax = plt.subplots(figsize=(figwidth, figheight))
    g = sns.heatmap(to_plot.T, ax=ax, cmap="mako")

    ax = g.axes
    ax.add_patch(Rectangle((x, y), 1, 1, fill=False, edgecolor="red", lw=3))
    ax.hlines(y + 0.5, 0, x, colors="red", linestyles="dashed")
    ymax = to_plot.shape[1]
    ax.vlines(x + 0.5, y + 1, ymax, colors="red", linestyles="dashed")
    if save is not None:
        plt.savefig(save)
