import numpy as np
import pandas as pd


def find_connected_components(contig_sorted):
    test = np.array(contig_sorted[["start", "end"]])
    overlaps = np.ones(len(test)) * -1
    ov_to = -1
    cur_comp = -1
    for i, x in enumerate(test):
        if x[0] > ov_to:  # we have left the component
            cur_comp += 1
            ov_to = x[1]
            overlaps[i] = cur_comp
        else:  # we are still in the component; let's see if we can extend its end
            overlaps[i] = cur_comp
            if x[1] > ov_to:
                ov_to = x[1]
    return overlaps


def representative(x):
    gene_values = np.unique(x)
    if len(gene_values) == 1 and gene_values[0] != -1:
        return gene_values[0]
    elif len(gene_values) == 1 and gene_values[0] == -1:
        return -1
    elif len(gene_values) == 2 and np.min(gene_values) == -1:
        return np.max(gene_values)
    else:
        return "complicated"


def summarize_contig(contig_sorted):
    comp_start = contig_sorted.groupby("component")["start"].min()
    comp_end = contig_sorted.groupby("component")["end"].max()
    dominant_gene = contig_sorted.groupby("component")["gene_index"].agg(
        lambda x: representative(x)
    )
    dominant_gene.name = "dominant"
    comp_k1m = contig_sorted.groupby("component")["trinity"].unique().str.join(",")

    contig_summary = pd.DataFrame([comp_start, comp_end, comp_k1m, dominant_gene]).T
    return contig_summary


def connectivity(summary, proximity_cutoff=300):
    merge_component = (np.arange(summary.shape[0]) + 1) * -1

    mc = 0
    for i, comp in enumerate(summary.index):
        if i == 0:
            continue
        current = summary.loc[comp]
        # find start of next entry
        after = i + 1
        after_starts = current.end + proximity_cutoff
        if after < summary.shape[0]:
            after_starts = summary.iloc[after].start
        # find end of previous entry
        before = i - 1
        before_ends = current.start - proximity_cutoff
        if before > -1:
            before_ends = summary.iloc[before].end

        # check if the previous entry is close enough
        if current.start - before_ends < proximity_cutoff:
            if merge_component[before] > -1:
                merge_component[i] = merge_component[before]
            else:
                merge_component[before] = mc
                merge_component[i] = merge_component[before]
                mc += 1

    summary["merge"] = merge_component
    return summary


def translate_to_gene(comp_summary, contig):
    gene_names = ["xxxxxxxxxxx"] * comp_summary.shape[0]
    for i, v in enumerate(comp_summary.dominant.values):
        gene = v
        if ",-1" in v:
            gene = v.split(",")[0]
        if "-1," in v:
            gene = v.split(",")[1]
        if v == "-1" or ("complicated" in v):
            gene = contig + "_" + str(i)
        gene_names[i] = gene
    return gene_names


def unpack_summary(summary, contig):
    summary.dominant = summary.dominant.astype(str)
    comp_groups = summary.groupby("merge")["dominant"].unique().str.join(",")
    comp_k1ms = summary.groupby("merge")["trinity"].unique().str.join(",")
    comp_summary = pd.DataFrame([comp_groups, comp_k1ms]).T
    comp_summary["gene"] = translate_to_gene(comp_summary, contig)

    s = comp_summary["trinity"].str.split(",").apply(pd.Series, 1).stack()
    s.index = s.index.droplevel(-1)
    s.name = "trinity"

    del comp_summary["trinity"]
    res = comp_summary.join(s)[["trinity", "gene"]].reset_index()
    del res["merge"]
    return res
