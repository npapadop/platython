import pickle
from pathlib import Path
import warnings
from tqdm import tqdm

from numba.core.errors import NumbaPerformanceWarning
import numpy as np
import scanpy as sc
import scipy

import harmonypy as hm

warnings.simplefilter(action="ignore", category=FutureWarning)
warnings.filterwarnings("ignore", category=NumbaPerformanceWarning)


def cluster_pipeline(adata, n_comps, clust_res, k, cluster_key="leiden"):
    adata.X = adata.layers["counts"].copy()

    # Normalize adata
    adata.X /= adata.obs["size_factors"].values[:, None]
    # immediately convert to csr else we have a matrix object at tidy.raw.X later
    adata.X = scipy.sparse.csr_matrix(adata.X)
    sc.pp.log1p(adata)

    adata.raw = adata

    sc.pp.highly_variable_genes(adata, n_top_genes=2000)
    sc.pp.scale(adata, zero_center=False)
    sc.pp.pca(adata, n_comps=n_comps, use_highly_variable=True, svd_solver="arpack")

    ho = hm.run_harmony(adata.obsm["X_pca"], adata.obs, ["batch"])

    adata.obsm["X_harmony"] = ho.Z_corr.T[:, :20]

    sc.pp.neighbors(adata, metric="cosine", use_rep="X_harmony", n_neighbors=k)
    sc.tl.leiden(adata, resolution=clust_res, key_added=cluster_key)


def cluster_to_jaccard(goal, prediction):
    true_length = len(np.unique(goal))
    pred_length = len(np.unique(prediction))
    tabulated = np.zeros((true_length, pred_length))
    for i, j in zip(goal, prediction):
        tabulated[int(i), int(j)] += 1

    true_clusters, true_freq = np.unique(goal, return_counts=True)
    pred_clusters, pred_freq = np.unique(prediction, return_counts=True)
    for i, freq_i in zip(true_clusters, true_freq):
        for j, freq_j in zip(pred_clusters, pred_freq):
            x = int(i)
            y = int(j)
            tabulated[x, y] /= freq_i + freq_j - tabulated[x, y]
    return tabulated


def calculate_stability(
    adata,
    n_comps,
    clust_res,
    k,
    clust_key="leiden",
    pipeline_func=cluster_pipeline,
    replicates=20,
    fraction=0.8,
):
    stability = np.zeros((replicates, len(adata.obs[clust_key].cat.categories)))
    prev_index = None
    for r in tqdm(range(replicates)):
        # subsample the data
        keep = adata.obs.groupby(clust_key).sample(frac=fraction, random_state=r).index
        subset = adata[keep].copy()
        # cluster the subsampled data
        pipeline_func(subset, n_comps, clust_res, k, cluster_key="predict")
        # compare the subsampled clustering to the the real one and keep the best
        # matching subsampled cluster
        goal = subset.obs[clust_key].values
        pred = subset.obs["predict"].values
        overview = cluster_to_jaccard(goal, pred)
        stability[r] = np.max(overview, axis=1)
    return stability


def evaluate_clustering(adata, stability, stability_cutoff=0.6):
    clusters, freq = np.unique(adata.obs.leiden.astype(int), return_counts=True)
    stable_clusters = np.mean(stability, axis=0) > stability_cutoff
    cells_in_stable = np.sum(freq[stable_clusters])

    return stable_clusters, cells_in_stable
