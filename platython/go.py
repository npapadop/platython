import requests
import time
import pandas as pd
import numpy as np


def _post_query_revigo(go_list):
    submission = "\n".join(go_list)

    payload = {
        "cutoff": "0.7",
        "valueType": "pvalue",
        "speciesTaxon": "0",
        "measure": "SIMREL",
        "goList": submission,
    }
    r = requests.post("http://revigo.irb.hr/StartJob", data=payload)

    jobid = r.json()["jobid"]

    # Check job status
    running = 1
    while running != 0:
        r = requests.get(f"http://revigo.irb.hr/QueryJob?jobid={jobid}&type=jstatus")
        running = r.json()["running"]
        time.sleep(1)
    return jobid


def _fetch_responses(jobid, namespaces=["1"], response_types=["RTreeMap"]):
    # Fetch results
    responses = {}
    for namespace in namespaces:
        for response_type in response_types:
            r = (
                requests.post(
                    f"http://revigo.irb.hr/QueryJob?jobid={jobid}&namespace={namespace}&type={response_type}"
                ),
            )
            responses[(namespace, response_type)] = r[0]

    return responses


def ask_revigo(go_list, namespaces=["1"], response_types=["RTreeMap"]):
    job_id = _post_query_revigo(go_list)
    responses = _fetch_responses(
        job_id, namespaces=namespaces, response_types=response_types
    )
    return responses


def write_revigo_treemap(response, title="revigo", out="treemap.R"):
    lines = response.text.split("\n")
    for i, l in enumerate(lines):
        if "title =" in l:
            lines[i] = f'title = "{title}",\r'
        if "pdf" in l:
            lines[
                i
            ] = 'png(file="revigo_treemap.png", width=16, height=9, units="in", res=600)\r'
        if "bg.labels" in l:
            lines[i] = "#" + l

    with open(out, "w") as f:
        for l in lines:
            f.write(l + "\n")


def read_go_terms(go_loc, gene_id="gene_id", species_prefix=""):
    go_terms = pd.read_csv(go_loc, sep="\t")
    go_terms[gene_id] = species_prefix + go_terms[gene_id]
    if "term" in go_terms.columns:
        go_terms.drop(columns=["term"], inplace=True)
    return go_terms


def prepare_go_analysis(
    go_terms, candidates, gene_id="gene_id", association_out="./association"
):
    keep = go_terms[gene_id].isin(candidates)
    study = set(go_terms[gene_id][keep])

    population = set(go_terms[gene_id])

    association = pd.DataFrame(
        go_terms.groupby(gene_id)["go_id"].apply(lambda x: ";".join(x))
    )
    association.reset_index().to_csv(
        association_out, header=False, index=False, sep="\t"
    )
    return study, population, association_out


def table_from_revigo(responses, namespace):
    table = responses[(namespace, "Table")].text.split("\n")[:-1]
    if len(table) == 1:
        return None
    for i, row in enumerate(table):
        table[i] = row.rstrip().split("\t")
        table[i][0] = table[i][0].replace('"', "")
        table[i][1] = table[i][1].replace('"', "")

    return pd.DataFrame(table[1:], columns=table[0])


def group_GO_terms(table, inplace=True):
    components = np.ones(table.shape[0], dtype=int) * -1
    for i, _ in enumerate(components):
        if i == 0:
            components[i] = i
        if table["PC_0"][i] == "null":
            components[i] = components[i - 1]
        else:
            components[i] = components[i - 1] + 1
    if inplace:
        table["components"] = components
    else:
        return components


def find_genes_by_go(terms, gene_map, adata, go_id="go_id", gene_id="gene_id"):
    keep = gene_map[go_id].isin(terms)
    genes = gene_map[gene_id][keep].values
    return np.intersect1d(genes, adata.var.index)


def get_go_enriched_genes(
    terms,
    query,
    target,
    target_species,
    query_go_terms,
    target_go_terms,
    celltype_from,
    celltype_to,
    max_genes=50,
):
    query_genes = find_genes_by_go(terms, query_go_terms, query)
    ranking = pd.DataFrame(query.uns["rank_genes_groups"]["names"])[celltype_from]
    ranking = ranking.reset_index().set_index(celltype_from).loc[query_genes]
    query_genes = ranking.sort_values(by="index")[:max_genes].index.values

    target_genes = find_genes_by_go(terms, target_go_terms, target)
    ranking = pd.DataFrame(target.uns["rank_genes_groups"]["names"])[celltype_to]
    if not ranking[0].startswith(target_species):
        ranking = target_species + "_" + ranking
    ranking = ranking.reset_index().set_index(celltype_to).loc[target_genes]
    target_genes = ranking.sort_values(by="index")[:max_genes].index.values

    query_genes = [[g, None] for g in query_genes]
    target_genes = [[None, g] for g in target_genes]
    go_connections = query_genes + target_genes

    return np.array(go_connections)


def go_plot_title(terms, godag):
    bla = godag[terms[0]]
    title = bla.namespace + "/" + bla.name
    return title
