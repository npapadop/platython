import scanpy as sc
from scipy.sparse import issparse


def pyScTransform(adata, output_file=None):
    """
    Function to call scTransform from Python (ad2seurat.py from https://github.com/normjam/benchmark)
    """
    import rpy2.robjects as ro
    import anndata2ri

    ro.r("library(Seurat)")
    ro.r("library(scater)")
    anndata2ri.activate()

    sc.pp.filter_genes(adata, min_cells=5)

    if issparse(adata.X):
        if not adata.X.has_sorted_indices:
            adata.X.sort_indices()

    for key in adata.layers:
        if issparse(adata.layers[key]):
            if not adata.layers[key].has_sorted_indices:
                adata.layers[key].sort_indices()

    ro.globalenv["adata"] = adata

    ro.r('seurat_obj = as.Seurat(adata, counts="X", data = NULL)')

    ro.r(
        "res <- SCTransform(object=seurat_obj, return.only.var.genes = FALSE, do.correct.umi = FALSE)"
    )

    norm_x = ro.r("res@assays$SCT@scale.data").T

    adata.layers["normalized"] = norm_x

    if output_file:
        adata.write(output_file)


def size_factors(adata):
    """
    Function to calculate size factors via `scran`.
    """
    import rpy2.robjects as ro
    import anndata2ri

    ro.r("suppressMessages(library(scran))")
    anndata2ri.activate()

    adata_pp = adata.copy()
    sc.pp.normalize_per_cell(adata_pp, counts_per_cell_after=1e6)
    sc.pp.log1p(adata_pp)
    sc.pp.pca(adata_pp, n_comps=50)

    sc.pp.neighbors(adata_pp, n_pcs=50)
    sc.tl.leiden(adata_pp, key_added="groups", resolution=0.5)

    # Preprocess variables for scran normalization
    ro.globalenv["input_groups"] = adata_pp.obs["groups"]
    ro.globalenv["data_mat"] = adata.X.T
    ro.r("sce <- SingleCellExperiment(list(counts=data_mat))")
    ro.r("sum.factors <- computeSumFactors(sce, clusters=input_groups, min.mean=0.1)")
    size_factors = ro.r("sizeFactors(sum.factors)")
    return size_factors


def PFlogPF(adata):
    """
    Implements the proportional fitting and log1p transformation proposed by Booeshagi et al.
    (2022). It will save the raw counts in a new layer, normalize .X to the median, log-transform,
    and then proportionally fit the log-transformed data (Booeshagi et al. bioRxiv 2022
    https://doi.org/10.1101/2022.05.06.490859)

    Parameters
    ----------
    adata : AnnData object
        the AnnData object to be normalized

    Returns
    -------
    None
    """
    adata.layers["counts"] = adata.X.copy()

    proportional_fitting = sc.pp.normalize_total(adata, target_sum=None, inplace=False)
    # log1p transform
    adata.layers["log1pPF_normalization"] = sc.pp.log1p(proportional_fitting["X"])
    # proportional fitting
    adata.X = sc.pp.normalize_total(
        adata, target_sum=None, layer="log1pPF_normalization", inplace=False
    )["X"]

    # adata.X = np.sqrt(adata.X)

    adata.raw = adata
