[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.8041483.svg)](https://doi.org/10.5281/zenodo.8041483)


This package contains my personal collection of code for various aspects of single-cell RNA-seq analysis. I hope to eventually transition certain aspects of it to standalone packages, but for now this is the best I can do.

## Highlights

### Actionable clustering

Code in the `cluster.py` module; for an example, see [TODO].

I subscribe to the [evolutionary theory](https://www.nature.com/articles/nrg.2016.127) of cell types, meaning that I expect cell types to have distinct expression programs. Whatever clustering I produce should lead to clusters that have a meaningful number (say, 20) of genes that are differentially expressed between them. One way to produce this is to overcluster the data and iteratively merge until all remaining clusters are transcriptionally distinct.

I got the original idea and code from [Jacob Musser](https://www.science.org/doi/10.1126/science.abj2949), and reimplemented it in Python (fixing some edge cases). It is very much in the mold of [Jean Fan](https://jef.works/blog/2018/02/28/stability-testing/) and [Valentine Svensson](https://www.nxn.se/valent/2018/3/5/actionable-scrna-seq-clusters), who articulated similar thoughts around that time.

### Plot coexpression in single cells

The `plot_coexpression()` [function](https://git.embl.de/npapadop/platython/-/blob/master/platython/plotting.py#L374); for an example, see [TODO].

A collaborator wanted to see if there were cells co-expressing their two favorite genes, to corroborate _in-situ_ hybridisations. I was unhappy with the solutions I found at the time (2020-ish?), so I wrote up something myself. 

### Paired dotplots

### 
